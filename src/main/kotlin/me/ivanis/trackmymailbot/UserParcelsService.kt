package me.ivanis.trackmymailbot

import me.ivanis.trackmymailbot.exception.BotException
import me.ivanis.trackmymailbot.model.UserParcelsTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.joda.time.Duration
import java.util.Date

class UserParcelsService(
    private val db: Database,
    private val messages: BotMessages,
    expirationIntervalDays: Long
) {
    private val expirationInterval = Duration.standardDays(expirationIntervalDays)

    fun getUserParcels(userId: Int): List<String> {
        return transaction(db) {
            UserParcelsTable
                .select { UserParcelsTable.userId eq userId }
                .map { it[UserParcelsTable.trackingNumber] }
                .toList()
        }
    }

    fun addToTrack(userId: Int, trackingNumber: String, lastStatusDate: Date? = null) {
        if (trackingNumberForUserExists(userId, trackingNumber)) {
            throw BotException(messages.get("common.tracking_number_already_added", trackingNumber))
        }
        val lastStatusDateTime = lastStatusDate?.let { DateTime(it.time) }
        val now = DateTime.now()
        transaction(db) {
            UserParcelsTable.insert {
                it[UserParcelsTable.userId] = userId
                it[UserParcelsTable.trackingNumber] = trackingNumber
                it[UserParcelsTable.lastUpdateDate] = now
                it[UserParcelsTable.addedDate] = now
                it[UserParcelsTable.lastStatusDate] = lastStatusDateTime
            }
        }
    }

    fun delete(userId: Int, trackingNumber: String) {
        if (!trackingNumberForUserExists(userId, trackingNumber)) {
            throw BotException(messages.get("common.tracking_number_not_added", trackingNumber))
        }
        transaction(db) {
            UserParcelsTable.deleteWhere {
                UserParcelsTable.userId.eq(userId) and UserParcelsTable.trackingNumber.eq(trackingNumber)
            }
        }
    }

    fun deleteExpiredTrackingNumbers() {
        transaction(db) {
            UserParcelsTable.deleteWhere {
                UserParcelsTable.addedDate less DateTime.now().minus(expirationInterval)
            }
        }
    }

    private fun trackingNumberForUserExists(userId: Int, trackingNumber: String): Boolean {
        return transaction(db) {
            UserParcelsTable
                .select {
                    UserParcelsTable.userId.eq(userId) and UserParcelsTable.trackingNumber.eq(trackingNumber)
                }
                .count() > 0
        }
    }
}
