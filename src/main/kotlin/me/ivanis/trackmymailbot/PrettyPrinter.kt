package me.ivanis.trackmymailbot

import me.ivanis.trackmymailbot.pochta.HistoryItem
import me.ivanis.trackmymailbot.pochta.TrackingItem
import java.text.SimpleDateFormat
import java.util.*

class PrettyPrinter(
    private val messages: BotMessages
) {
    private val from = messages["template.from"]
    private val fromWhom = messages["template.whom"]
    private val to = messages["template.to"]
    private val where = messages["template.where"]
    private val weight = messages["template.weight"]
    private val weightUnit = messages["template.weightUnit"]

    fun prettyPrint(trackingItem: TrackingItem, trackingNumber: String): String {
        return """
        |📦 *${trackingItem.title ?: trackingNumber}*
        |*$from* ${safePrint(trackingItem.originCountryName)}
        |*$fromWhom* ${safePrint(trackingItem.sender)}
        |*$to* ${safePrint(trackingItem.recipient)}
        |*$where* ${printDestination(trackingItem.destinationCountryName, trackingItem.destinationCityName)}
        |*$weight* ${printWeight(trackingItem)}
        |
        |${prettyPrint(trackingItem.trackingHistoryItemList)}
        """.trimMargin()

    }

    fun prettyPrint(historyItems: List<HistoryItem>?): String {
        if (historyItems == null) {
            return messages["common.no_tracking_info"]
        }
        val sortedHistoryItems = historyItems.sortedBy { it.date }
        return sortedHistoryItems.joinToString(separator = "\n\n", transform = ::prettyPrintHistoryItem)
    }

    private fun printWeight(trackingItem: TrackingItem): String {
        val trackingItemWithWeight = trackingItem.trackingHistoryItemList?.firstOrNull { it.weight != null }
        return trackingItemWithWeight?.let { "${it.weight} $weightUnit" } ?: messages["common.no_info"]
    }

    private fun safePrint(s: String?): String {
        return if (s.isNullOrBlank()) {
            messages["common.no_info"]
        } else {
            s
        }
    }

    private fun printDestination(destinationCountryName: String?, destinationCityName: String?): String {
        val result = arrayOf(destinationCountryName, destinationCityName)
            .asSequence()
            .filter { !it.isNullOrBlank() }
            .joinToString(separator = ", ")
        return if (result.isBlank()) {
            messages["common.no_info"]
        } else {
            result
        }
    }

    private fun prettyPrintHistoryItem(historyItem: HistoryItem): String {
        return """
            |🚚 ${historyItem.humanStatus}
            |📍 ${printLocation(historyItem.countryName, historyItem.cityName, historyItem.index)}
            |🗓 ${printDate(historyItem.date)}
            """.trimMargin()
    }

    private fun printLocation(countryName: String?, cityName: String?, index: String?): String {
        return arrayOf(countryName, cityName, index)
            .asSequence()
            .filter { !it.isNullOrBlank() }
            .joinToString(separator = ", ")
    }

    private fun printDate(date: Date?): String = date?.let(dateFormat::format) ?: ""

    companion object {
        private val dateFormat = SimpleDateFormat("dd MMMM yyyy HH:mm", Locale("ru"))
    }
}
