package me.ivanis.trackmymailbot.tasks

import me.ivanis.trackmymailbot.BotMessages
import me.ivanis.trackmymailbot.PrettyPrinter
import me.ivanis.trackmymailbot.TrackMyMailBot
import me.ivanis.trackmymailbot.TrackingService
import me.ivanis.trackmymailbot.model.UserParcelsTable
import me.ivanis.trackmymailbot.pochta.HistoryItem
import me.ivanis.trackmymailbot.pochta.ParcelStatusResponse
import me.ivanis.trackmymailbot.pochta.PochtaClient
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.slf4j.LoggerFactory
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import java.util.concurrent.TimeUnit
import kotlin.system.measureTimeMillis

class UpdateStatusTask(
    private val db: Database,
    private val pochtaClient: PochtaClient,
    private val prettyPrinter: PrettyPrinter,
    private val trackMyMailBot: TrackMyMailBot,
    private val messages: BotMessages,
    private val requestDelay: Long
) {
    private val log = LoggerFactory.getLogger(TrackingService::class.java)

    fun updateParcelStatuses() {
        log.info("Update the statuses of parcels started!")
        val elapsed = measureTimeMillis {
            transaction(db) {
                UserParcelsTable.selectAll()
                    .map(::toUserParcel)
                    .forEach(::updateParcelStatus)
            }
        }
        log.info("Parcel statuses updated in $elapsed ms")
    }

    private fun updateParcelStatus(userParcel: UserParcel) {
        val pochtaResponse = pochtaClient.track(userParcel.trackingNumber)
        if (pochtaResponse is ParcelStatusResponse) {
            val trackingHistory = pochtaResponse.list.firstOrNull()?.trackingItem?.trackingHistoryItemList
            val lastStatusDateInDb = userParcel.lastStatusDate ?: DateTime(0)
            val newStatus = trackingHistory
                ?.asSequence()
                ?.filter { DateTime(it.date).isAfter(lastStatusDateInDb) }
                ?.sortedBy { it.date }
                .orEmpty()
                .toList()
            val newLastStatus = newStatus.lastOrNull()
            if (newLastStatus != null) {
                updateStatusDateInDb(userParcel, DateTime(newLastStatus.date))
                sendNewStatus(userParcel.trackingNumber, userParcel.userId, newStatus)
            }
            TimeUnit.SECONDS.sleep(requestDelay)
        }
    }

    private fun updateStatusDateInDb(userParcel: UserParcel, newLastStatusDate: DateTime) = transaction(db) {
        UserParcelsTable.update(
            where = {
                UserParcelsTable.userId.eq(userParcel.userId).and(
                    UserParcelsTable.trackingNumber.eq(userParcel.trackingNumber)
                )
            },
            body = {
                it[lastStatusDate] = newLastStatusDate
                it[lastUpdateDate] = DateTime.now()
            })
    }

    private fun sendNewStatus(trackingNumber: String, userId: Int, historyItems: List<HistoryItem>) {
        val message = messages.get("common.new_status", trackingNumber, prettyPrinter.prettyPrint(historyItems))
        val sendMessage = SendMessage(userId.toLong(), message)
        trackMyMailBot.execute(sendMessage)
    }

    private fun toUserParcel(userParcelRow: ResultRow): UserParcel {
        return UserParcel(
            userParcelRow[UserParcelsTable.userId],
            userParcelRow[UserParcelsTable.trackingNumber],
            userParcelRow[UserParcelsTable.lastUpdateDate],
            userParcelRow[UserParcelsTable.lastStatusDate]
        )
    }

    private data class UserParcel(
        val userId: Int,
        val trackingNumber: String,
        val lastUpdateDate: DateTime?,
        val lastStatusDate: DateTime?
    )
}
