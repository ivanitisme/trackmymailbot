package me.ivanis.trackmymailbot.tasks

import me.ivanis.trackmymailbot.UserParcelsService
import org.slf4j.LoggerFactory
import kotlin.system.measureTimeMillis

class ClearExpiredTask(private val userParcelsService: UserParcelsService) {
    private val log = LoggerFactory.getLogger(ClearExpiredTask::class.java)

    fun clearExpiredTrackingNumbers() {
        log.info("Clearing of expired tracking numbers started!")
        val elapsed = measureTimeMillis {
            userParcelsService.deleteExpiredTrackingNumbers()
        }
        log.info("Clearing of expired tracking numbers completed in $elapsed ms")
    }
}
