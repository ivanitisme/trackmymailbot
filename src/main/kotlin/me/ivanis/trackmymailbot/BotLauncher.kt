package me.ivanis.trackmymailbot

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import me.ivanis.trackmymailbot.config.BotConfig
import me.ivanis.trackmymailbot.model.UserParcelsTable
import me.ivanis.trackmymailbot.pochta.PochtaClient
import me.ivanis.trackmymailbot.pochta.SimplePochtaClient
import me.ivanis.trackmymailbot.tasks.ClearExpiredTask
import me.ivanis.trackmymailbot.tasks.UpdateStatusTask
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.slf4j.LoggerFactory
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.meta.TelegramBotsApi
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

class BotLauncher {
    private val log = LoggerFactory.getLogger(BotLauncher::class.java)

    fun launch() {
        val config = BotConfig.load()
        val messages = BotMessages()

        val db = initDatabase(config)

        transaction(db) {
            SchemaUtils.create(UserParcelsTable)
        }
        val pochtaClient: PochtaClient = SimplePochtaClient()
        val prettyPrinter = PrettyPrinter(messages)
        val userParcelsService = UserParcelsService(db, messages, config.parcelExpirationIntervalDays)
        val trackingService = TrackingService(userParcelsService, pochtaClient, prettyPrinter, messages)


        ApiContextInitializer.init()
        val botsApi = TelegramBotsApi()

        val bot = TrackMyMailBot(config, trackingService, messages)
        botsApi.registerBot(bot)
        val trackingTask =
            UpdateStatusTask(db, pochtaClient, prettyPrinter, bot, messages, config.pochtaRequestDelaySeconds)
        val clearExpiredTask = ClearExpiredTask(userParcelsService)
        val executor = Executors.newSingleThreadScheduledExecutor()

        startUpdateParcelStatusesBackground(executor, trackingTask, config)
        startClearExpiredTrackingNumbersBackground(executor, clearExpiredTask, config)
    }

    private fun startClearExpiredTrackingNumbersBackground(
        executor: ScheduledExecutorService,
        clearExpiredTask: ClearExpiredTask,
        config: BotConfig
    ) {
        executor.scheduleWithFixedDelay(
            safeTask(clearExpiredTask::clearExpiredTrackingNumbers),
            config.clearExpiredDelayDays,
            config.clearExpiredDelayDays,
            TimeUnit.DAYS
        )
    }

    private fun startUpdateParcelStatusesBackground(
        executor: ScheduledExecutorService,
        trackingTask: UpdateStatusTask,
        config: BotConfig
    ) {
        executor.scheduleWithFixedDelay(
            safeTask(trackingTask::updateParcelStatuses),
            config.updateParcelDelayHours,
            config.updateParcelDelayHours,
            TimeUnit.HOURS
        )
    }

    private fun initDatabase(config: BotConfig): Database {
        val dbConfig = HikariConfig().apply {
            username = config.dbUsername
            password = config.dbPassword
            jdbcUrl = config.dbUrl
            driverClassName = config.dbDriver
        }

        return Database.connect(HikariDataSource(dbConfig))
    }

    private fun safeTask(block: () -> Unit): () -> Unit {
        return {
            try {
                block()
            } catch (e: Exception) {
                log.error("An error occurred while executing task", e)
            }
        }
    }
}

fun main() {
    BotLauncher().launch()
}
