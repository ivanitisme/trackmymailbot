package me.ivanis.trackmymailbot.config

data class BotConfig(
    val username: String,
    val token: String,
    val updateParcelDelayHours: Long,
    val clearExpiredDelayDays: Long,
    val parcelExpirationIntervalDays: Long,
    val pochtaRequestDelaySeconds: Long,
    val dbUrl: String,
    val dbDriver: String,
    val dbUsername: String,
    val dbPassword: String
) {
    companion object {
        // telegram
        private const val ENV_VAR_TOKEN = "BOT_TOKEN"
        private const val ENV_VAR_USERNAME = "BOT_USERNAME"
        // bot
        private const val ENV_VAR_UPDATE_STATUS_DELAY_HOURS = "BOT_UPDATE_STATUS_DELAY_HOURS"
        private const val ENV_VAR_CLEAR_DELAY_DAYS = "BOT_CLEAR_DELAY_DAYS"
        private const val ENV_VAR_PARCEL_EXPIRATION_INTERVAL_DAYS = "BOT_PARCEL_EXPIRATION_INTERVAL_DAYS"
        private const val ENV_VAR_POCHTA_REQUEST_DELAY_SECONDS = "BOT_POCHTA_REQUEST_DELAY_SECONDS"
        // db
        private const val ENV_VAR_DB_URL = "DB_URL"
        private const val ENV_VAR_DB_DRIVER = "DB_DRIVER"
        private const val ENV_VAR_DB_USERNAME = "DB_USERNAME"
        private const val ENV_VAR_DB_PASSWORD = "DB_PASSWORD"


        fun load(): BotConfig {
            val token = getEnvVar(ENV_VAR_TOKEN)
            val username = getEnvVar(ENV_VAR_USERNAME)
            val updateParcelDelayHours = getEnvVar(ENV_VAR_UPDATE_STATUS_DELAY_HOURS, "6").toLong()
            val clearExpiredDelayDays = getEnvVar(ENV_VAR_CLEAR_DELAY_DAYS, "1").toLong()
            val parcelExpirationIntervalDays = getEnvVar(ENV_VAR_PARCEL_EXPIRATION_INTERVAL_DAYS, "30").toLong()
            val pochtaRequestDelaySeconds = getEnvVar(ENV_VAR_POCHTA_REQUEST_DELAY_SECONDS, "5").toLong()
            val dbUrl = getEnvVar(ENV_VAR_DB_URL)
            val dbDriver = getEnvVar(ENV_VAR_DB_DRIVER)
            val dbUsername = getEnvVar(ENV_VAR_DB_USERNAME)
            val dbPassword = getEnvVar(ENV_VAR_DB_PASSWORD)

            return BotConfig(
                username = username,
                token = token,
                updateParcelDelayHours = updateParcelDelayHours,
                clearExpiredDelayDays = clearExpiredDelayDays,
                parcelExpirationIntervalDays = parcelExpirationIntervalDays,
                pochtaRequestDelaySeconds = pochtaRequestDelaySeconds,
                dbUrl = dbUrl,
                dbDriver = dbDriver,
                dbUsername = dbUsername,
                dbPassword = dbPassword
            )
        }

        private fun getEnvVar(name: String, default: String): String {
            return System.getenv(name) ?: default
        }

        private fun getEnvVar(name: String): String {
            return System.getenv(name)
                ?: throw RuntimeException("""The environment variable "$name" must be defined""")
        }
    }
}
