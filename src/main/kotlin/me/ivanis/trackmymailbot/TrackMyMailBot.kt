package me.ivanis.trackmymailbot

import me.ivanis.trackmymailbot.commands.*
import me.ivanis.trackmymailbot.config.BotConfig
import org.slf4j.LoggerFactory
import org.telegram.telegrambots.extensions.bots.commandbot.TelegramLongPollingCommandBot
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Update

class TrackMyMailBot(
    private val botConfig: BotConfig,
    private val trackingService: TrackingService,
    messages: BotMessages
) : TelegramLongPollingCommandBot(botConfig.username) {
    private val log = LoggerFactory.getLogger(TrackMyMailBot::class.java)

    init {
        val lsCommand = LsCommand(trackingService, messages)

        register(StartCommand(messages))
        register(HelpCommand(messages))
        register(AddCommand(trackingService, messages))
        register(TrackCommand(trackingService, messages))
        register(lsCommand)
        register(ListCommand(lsCommand, messages))
        register(DeleteCommand(trackingService, messages))
    }

    override fun getBotToken() = botConfig.token

    override fun processNonCommandUpdate(update: Update) {
        log.info(update.toString())
        if (update.hasMessage()) {
            val botResponseText = trackingService.track(update.message.text.toUpperCase())
            val answer = SendMessage(update.message.chatId, botResponseText)
                .enableMarkdown(true)
            execute(answer)
        }
    }
}
