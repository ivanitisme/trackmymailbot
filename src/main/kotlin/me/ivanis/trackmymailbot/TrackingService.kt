package me.ivanis.trackmymailbot

import me.ivanis.trackmymailbot.pochta.*
import org.slf4j.LoggerFactory


class TrackingService(
    private val userParcelsService: UserParcelsService,
    private val pochtaClient: PochtaClient,
    private val prettyPrinter: PrettyPrinter,
    private val messages: BotMessages
) {
    private val log = LoggerFactory.getLogger(TrackingService::class.java)

    fun track(trackingNumber: String): String {
        return when (val pochtaResponse = pochtaClient.track(trackingNumber)) {
            is ParcelStatusResponse -> prettyPrint(pochtaResponse, trackingNumber)
            is ErrorResponse        -> resolveError(pochtaResponse)
            is EmptyResponse        -> emptyResponse(trackingNumber)
        }
    }

    fun addToTrack(userId: Int, trackingNumber: String): String {
        return when (val parcelStatus = pochtaClient.track(trackingNumber)) {
            is ParcelStatusResponse -> {
                val trackingHistory = parcelStatus.list.firstOrNull()?.trackingItem?.trackingHistoryItemList
                val lastStatusDate = trackingHistory?.firstOrNull()?.date
                userParcelsService.addToTrack(userId, trackingNumber, lastStatusDate)
                messages.get("common.track_number_added", prettyPrinter.prettyPrint(trackingHistory))
            }
            is ErrorResponse        -> resolveError(parcelStatus)
            is EmptyResponse        -> emptyResponse(trackingNumber)
        }
    }

    fun deleteTrackingNumber(userId: Int, trackingNumber: String) {
        userParcelsService.delete(userId, trackingNumber)
    }

    fun getUserParcels(userId: Int): List<String> {
        return userParcelsService.getUserParcels(userId)
    }

    private fun emptyResponse(trackingNumber: String): String {
        log.info("Empty response from pochta.ru for $trackingNumber")
        return messages.get("common.no_info_track_number", trackingNumber)
    }

    private fun prettyPrint(parcelStatusResponse: ParcelStatusResponse, trackingNumber: String): String {
        val trackingItem: TrackingItem? = parcelStatusResponse.list.firstOrNull()?.trackingItem
        if (trackingItem != null) {
            return prettyPrinter.prettyPrint(trackingItem, trackingNumber)
        }
        return messages["common.no_info"]
    }

    private fun resolveError(errorResponse: ErrorResponse): String {
        if (errorResponse.error.description?.contains("Barcode validation failed") == true) {
            return messages["common.tracking_number_not_valid"]
        }
        return messages.get("common.pochta_error", errorResponse.toString())
    }
}
