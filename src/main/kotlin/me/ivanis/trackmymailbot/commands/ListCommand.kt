package me.ivanis.trackmymailbot.commands


import me.ivanis.trackmymailbot.BotMessages
import org.telegram.telegrambots.meta.api.objects.Chat
import org.telegram.telegrambots.meta.api.objects.User
import org.telegram.telegrambots.meta.bots.AbsSender

class ListCommand(
    private val lsCommand: LsCommand,
    messages: BotMessages
) : BaseCommand(messages["command.list.name"], messages["command.list.description"], messages) {
    override fun safeExecute(absSender: AbsSender, user: User, chat: Chat, args: Array<out String>) {
        lsCommand.execute(absSender, user, chat, args)
    }
}
