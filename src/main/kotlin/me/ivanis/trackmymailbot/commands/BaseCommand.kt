package me.ivanis.trackmymailbot.commands

import me.ivanis.trackmymailbot.BotMessages
import me.ivanis.trackmymailbot.exception.BotException
import org.slf4j.LoggerFactory
import org.telegram.telegrambots.extensions.bots.commandbot.commands.BotCommand
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Chat
import org.telegram.telegrambots.meta.api.objects.User
import org.telegram.telegrambots.meta.bots.AbsSender

abstract class BaseCommand(
    private val commandName: String,
    description: String,
    private val messages: BotMessages
) : BotCommand(commandName, description) {
    private val log = LoggerFactory.getLogger(BaseCommand::class.java)

    override fun execute(absSender: AbsSender, user: User, chat: Chat, arguments: Array<out String>) {
        log.info("Command $commandName, user: $user, args: ${arguments.joinToString(", ")}")
        try {
            safeExecute(absSender, user, chat, arguments)
        } catch (botException: BotException) {
            log.info(botException.message, botException)
            absSender.execute(SendMessage(chat.id, botException.message))
        } catch (e: Exception) {
            log.error("Error during the execution of the $commandName command for user $user", e)
            absSender.execute(SendMessage(chat.id, messages.get("command.error", commandName)))
        }

    }

    protected abstract fun safeExecute(absSender: AbsSender, user: User, chat: Chat, args: Array<out String>)
}
