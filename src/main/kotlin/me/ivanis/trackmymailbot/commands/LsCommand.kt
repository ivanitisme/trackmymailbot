package me.ivanis.trackmymailbot.commands

import me.ivanis.trackmymailbot.BotMessages
import me.ivanis.trackmymailbot.TrackingService
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Chat
import org.telegram.telegrambots.meta.api.objects.User
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow
import org.telegram.telegrambots.meta.bots.AbsSender

class LsCommand(
    private val trackingService: TrackingService,
    private val messages: BotMessages
) : BaseCommand(messages["command.ls.name"], messages["command.ls.description"], messages) {
    override fun safeExecute(absSender: AbsSender, user: User, chat: Chat, args: Array<out String>) {
        val trackingNumbers = trackingService.getUserParcels(user.id)
        if (trackingNumbers.isNullOrEmpty()) {
            val answer = SendMessage(chat.id, messages["common.no_parcels"])
            answer.enableMarkdown(true)
            absSender.execute(answer)
            return
        }

        val answer = SendMessage(chat.id, messages["common.parcels"]).apply {
            replyMarkup = createReplyKeyboard(trackingNumbers)
        }
        absSender.execute(answer)
    }

    private fun createReplyKeyboard(trackingNumbers: List<String>): ReplyKeyboardMarkup {
        return ReplyKeyboardMarkup().apply {
            keyboard = trackingNumbers.map { KeyboardRow().apply { add(it) } }
        }
    }
}
