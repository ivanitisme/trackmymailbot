package me.ivanis.trackmymailbot.commands

import me.ivanis.trackmymailbot.BotMessages
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Chat
import org.telegram.telegrambots.meta.api.objects.User
import org.telegram.telegrambots.meta.bots.AbsSender

class HelpCommand(
    private val messages: BotMessages
) : BaseCommand(messages["command.help.name"], messages["command.help.description"], messages) {
    override fun safeExecute(absSender: AbsSender, user: User, chat: Chat, args: Array<out String>) {
        val sendMessage = SendMessage(chat.id, messages["command.help.text"]).enableMarkdown(true)
        absSender.execute(sendMessage)
    }
}
