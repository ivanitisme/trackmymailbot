package me.ivanis.trackmymailbot.commands

import me.ivanis.trackmymailbot.BotMessages
import me.ivanis.trackmymailbot.TrackingService
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Chat
import org.telegram.telegrambots.meta.api.objects.User
import org.telegram.telegrambots.meta.bots.AbsSender

class AddCommand(
    private val trackingService: TrackingService,
    private val messages: BotMessages
) : BaseCommand(messages["command.add.name"], messages["command.add.description"], messages) {
    override fun safeExecute(absSender: AbsSender, user: User, chat: Chat, args: Array<out String>) {
        if (args.isNotEmpty() && args.size == 1) {
            val status = trackingService.addToTrack(user.id, args[0])
            val answer = SendMessage(chat.id, status)
            answer.enableMarkdown(true)
            absSender.execute(answer)
        } else {
            absSender.execute(SendMessage(chat.id, messages["common.argument_not_valid"]))
        }
    }
}
