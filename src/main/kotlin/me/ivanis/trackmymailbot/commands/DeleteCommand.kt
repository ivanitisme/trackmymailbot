package me.ivanis.trackmymailbot.commands

import me.ivanis.trackmymailbot.BotMessages
import me.ivanis.trackmymailbot.TrackingService
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Chat
import org.telegram.telegrambots.meta.api.objects.User
import org.telegram.telegrambots.meta.bots.AbsSender

class DeleteCommand(
    private val trackingService: TrackingService,
    private val messages: BotMessages
) : BaseCommand(messages["command.del.name"], messages["command.del.description"], messages) {
    override fun safeExecute(absSender: AbsSender, user: User, chat: Chat, args: Array<out String>) {
        if (args.isNotEmpty() && args.size == 1) {
            trackingService.deleteTrackingNumber(user.id, args[0])
            absSender.execute(SendMessage(chat.id, messages["common.deleted"]))
        } else {
            absSender.execute(SendMessage(chat.id, messages["common.argument_not_valid"]))
        }
    }
}
