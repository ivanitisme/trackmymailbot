package me.ivanis.trackmymailbot.model

import org.jetbrains.exposed.dao.IntIdTable

object UserParcelsTable : IntIdTable() {
    val userId = integer("user_id")
    val trackingNumber = varchar("tracking_number", length = 20)
    val addedDate = datetime("added_date")
    val lastStatusDate = datetime("last_status_date").nullable()
    val lastUpdateDate = datetime("last_update_date").nullable()

    init {
        uniqueIndex(userId, trackingNumber)
    }
}
