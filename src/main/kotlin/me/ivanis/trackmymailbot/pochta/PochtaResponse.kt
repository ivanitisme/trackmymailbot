package me.ivanis.trackmymailbot.pochta

import java.util.*

sealed class PochtaResponse

data class ErrorResponse(
    val error: Error
) : PochtaResponse()

data class Error(
    val code: Int,
    val description: String?
)

data class ParcelStatusResponse(
    val list: List<TrackingItemInfo>
) : PochtaResponse()

object EmptyResponse : PochtaResponse()

data class TrackingItemInfo(
    val officeSummary: OfficeSummary?,
    val trackingItem: TrackingItem?
)

data class TrackingItem(
    val sender: String?,
    val originCountryName: String?,
    val destinationCityName: String?,
    val destinationCountryName: String?,
    val recipient: String?,
    val title: String?,
    val weight: Int?,
    val mailType: String?,
    val trackingHistoryItemList: List<HistoryItem>?
)

data class HistoryItem(
    val date: Date?,
    val humanStatus: String?,
    val cityName: String?,
    val countryName: String?,
    val index: String?,
    val description: String?,
    val weight: Int?
)

data class OfficeSummary(
    val postalCode: String?,
    val addressSource: String?,
    val latitude: Double?,
    val longitude: Double?,
    val officeMapUrl: String?,
    val officeMapImageUrl: String?
)
