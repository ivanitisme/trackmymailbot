package me.ivanis.trackmymailbot.pochta

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.slf4j.LoggerFactory

interface PochtaClient {
    fun track(trackingNumber: String): PochtaResponse
}

class SimplePochtaClient : PochtaClient {
    private val log = LoggerFactory.getLogger(SimplePochtaClient::class.java)
    private val client = OkHttpClient()
    private val mapper = jacksonObjectMapper()

    init {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }

    override fun track(trackingNumber: String): PochtaResponse {
        val serviceResponse: Response = client
            .newCall(createRequest(trackingNumber))
            .execute()

        val responseString: String? = serviceResponse.body?.string()
        log.debug("pochta.ru response: $responseString")
        return if (responseString.isNullOrBlank()) {
            EmptyResponse
        } else {
            parseResponse(responseString)
        }
    }

    private fun createRequest(trackingNumber: String): Request {
        return Request.Builder()
            .url(createUrl(trackingNumber))
            .header("Accept-Language", "ru-ru")
            .get()
            .build()
    }

    private fun parseResponse(json: String): PochtaResponse {
        val responseNode: JsonNode = mapper.readTree(json)
        return when {
            responseNode.has("list")  -> mapper.treeToValue(responseNode, ParcelStatusResponse::class.java)
            responseNode.has("error") -> mapper.treeToValue(responseNode, ErrorResponse::class.java)
            else                      -> throw RuntimeException("Unknown pochta response")
        }
    }

    private fun createUrl(trackingNumber: String): String {
        return "https://www.pochta.ru/tracking" +
                "?p_p_id=trackingPortlet_WAR_portalportlet&" +
                "p_p_lifecycle=2&" +
                "p_p_resource_id=getList&" +
                "p_p_cacheability=cacheLevelPage&" +
                "barcodeList=$trackingNumber"
    }
}
