package me.ivanis.trackmymailbot

import java.io.InputStreamReader
import java.text.MessageFormat
import java.util.*


class BotMessages {
    private val messagesProperties: Properties = loadMessageProperties()

    fun get(key: String, vararg args: String): String {
        val messageTemplate = messagesProperties.getProperty(key)
            ?: throw RuntimeException("Message with key=$key not found")
        return MessageFormat.format(messageTemplate, *args)
    }

    operator fun get(key: String): String {
        return messagesProperties.getProperty(key)
            ?: throw RuntimeException("Message with key=$key not found")
    }

    companion object {
        fun loadMessageProperties(): Properties {
            val props = Properties()
            val resource = BotMessages::class.java.classLoader.getResource("messages.properties")
            props.load(InputStreamReader(resource!!.openStream(), "UTF8"))
            return props
        }
    }
}
