package me.ivanis.trackmymailbot.exception

class BotException(message: String? = null, reason: Throwable? = null) : RuntimeException(message, reason)
