package me.ivanis.trackmymailbot

import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import me.ivanis.trackmymailbot.exception.BotException
import me.ivanis.trackmymailbot.model.UserParcelsTable

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class TrackingServiceTest : BaseTest() {
    private val trackingService: TrackingService
    private val messages = BotMessages()

    init {
        val userParcelsService = UserParcelsService(db, messages, 1)
        val prettyPrinter = PrettyPrinter(messages)
        val pochtaClient = TestPochtaClient()

        trackingService = TrackingService(
            userParcelsService = userParcelsService,
            messages = messages,
            prettyPrinter = prettyPrinter,
            pochtaClient = pochtaClient
        )
    }

    @AfterEach
    private fun clearDb() {
        transaction(db) {
            UserParcelsTable.deleteAll()
        }
    }

    @Test
    fun track() {
        val status = trackingService.track("RP000000000CN")
        val expectedStatus = """
        📦 *Мелкий пакет из Китая*
        *Откуда:* Китай
        *От кого:* Wang
        *Кому:* Иван
        *Куда:* Россия, Новосибирск
        *Вес:* 100 г.

        🚚 Выпущено таможней
        📍 Россия, Обь, 600001
        🗓 01 января 2018 00:00

        🚚 Прибыло в место вручения
        📍 Россия, Новосибирск, 600002
        🗓 02 января 2018 00:00

        🚚 Получено адресатом
        📍 Россия, Новосибирск, 600003
        🗓 03 января 2018 00:00
        """.trimIndent()
        assertEquals(expectedStatus, status)
    }

    @Test
    fun trackErrorCode() {
        val status = trackingService.track("asd")
        assertEquals("Невалидный трек код", status)
    }

    @Test
    fun trackNoStatus() {
        val status = trackingService.track("RP000000001CN")
        assertEquals(messages["common.no_info"], status)
    }

    @Test
    fun addToTrack() {
        val userId = 1
        val trackingNumber = "RP000000000CN"

        trackingService.addToTrack(userId = userId, trackingNumber = trackingNumber)
        assertEquals(true, isParcelPresent(userId, trackingNumber))
    }

    @Test
    fun deleteTrackingNumber() {
        val userId = 1
        val trackingNumber = "RP000000000CN"

        trackingService.addToTrack(userId = userId, trackingNumber = trackingNumber)
        trackingService.deleteTrackingNumber(userId = userId, trackingNumber = trackingNumber)
        assertEquals(false, isParcelPresent(userId, trackingNumber))
    }

    @Test
    fun deleteNonExistingTrackingNumber() {
        assertThrows(BotException::class.java) {
            trackingService.deleteTrackingNumber(userId = 1, trackingNumber = "RP000000000CN")
        }
    }

    @Test
    fun getUserParcels() {
        val userId = 1
        val trackingNumber0 = "RP000000000CN"
        val trackingNumber1 = "RP000000001CN"
        val trackingNumber2 = "RP000000002CN"

        assertEquals(emptyList<String>(), trackingService.getUserParcels(userId = userId))

        trackingService.addToTrack(userId = userId, trackingNumber = trackingNumber0)
        assertEquals(setOf(trackingNumber0), trackingService.getUserParcels(userId = userId).toSet())

        trackingService.addToTrack(userId = userId, trackingNumber = trackingNumber1)
        trackingService.addToTrack(userId = userId, trackingNumber = trackingNumber2)
        val expected = setOf(trackingNumber0, trackingNumber1, trackingNumber2)
        assertEquals(expected, trackingService.getUserParcels(userId = userId).toSet())
    }

    private fun isParcelPresent(userId: Int, trackingNumber: String): Boolean {
        return transaction(db) {
            UserParcelsTable
                .select(
                    where = {
                        UserParcelsTable.userId.eq(userId) and UserParcelsTable.trackingNumber.eq(trackingNumber)
                    })
                .count() == 1
        }
    }
}
