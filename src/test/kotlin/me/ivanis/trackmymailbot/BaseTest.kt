package me.ivanis.trackmymailbot

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import me.ivanis.trackmymailbot.model.UserParcelsTable


abstract class BaseTest {
    protected val db: Database = initDatabase()

    init {
        transaction(db) {
            SchemaUtils.create(UserParcelsTable)
        }
    }

    private fun initDatabase(): Database {
        return Database.connect(HikariDataSource(HikariConfig().apply {
            jdbcUrl = "jdbc:h2:mem:test"
            driverClassName = "org.h2.Driver"
        }))
    }
}
