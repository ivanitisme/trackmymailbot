package me.ivanis.trackmymailbot

import me.ivanis.trackmymailbot.pochta.*
import java.util.*

class TestPochtaClient : PochtaClient {
    override fun track(trackingNumber: String): PochtaResponse {
        return when (trackingNumber) {
            "RP000000000CN" -> createParcelStatusResponse()
            "asd" -> ErrorResponse(error = Error(code = 500, description = "Barcode validation failed"))
            else -> ParcelStatusResponse(listOf(TrackingItemInfo(null, null)))
        }
    }

    private fun createParcelStatusResponse(): ParcelStatusResponse {
        val trackingItemInfo = TrackingItemInfo(
            officeSummary = OfficeSummary(
                postalCode = null,
                addressSource = null,
                latitude = null,
                longitude = null,
                officeMapUrl = null,
                officeMapImageUrl = null
            ),
            trackingItem = TrackingItem(
                sender = "Wang",
                originCountryName = "Китай",
                destinationCityName = "Новосибирск",
                destinationCountryName = "Россия",
                recipient = "Иван",
                title = "Мелкий пакет из Китая",
                weight = null,
                mailType = "Мелкий пакет",
                trackingHistoryItemList = listOf(
                    HistoryItem(
                        date = GregorianCalendar(2018, 0, 1).time,
                        humanStatus = "Выпущено таможней",
                        cityName = "Обь",
                        countryName = "Россия",
                        index = "600001",
                        description = "Выпущено таможней",
                        weight = 100
                    ),
                    HistoryItem(
                        date = GregorianCalendar(2018, 0, 2).time,
                        humanStatus = "Прибыло в место вручения",
                        cityName = "Новосибирск",
                        countryName = "Россия",
                        index = "600002",
                        description = "Прибыло в место вручения",
                        weight = 100
                    ),
                    HistoryItem(
                        date = GregorianCalendar(2018, 0, 3).time,
                        humanStatus = "Получено адресатом",
                        cityName = "Новосибирск",
                        countryName = "Россия",
                        index = "600003",
                        description = "Получено адресатом",
                        weight = 100
                    )
                )
            )
        )
        return ParcelStatusResponse(list = listOf(trackingItemInfo))
    }
}
